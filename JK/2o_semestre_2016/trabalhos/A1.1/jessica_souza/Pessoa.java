package sistemadecadastro;
import java.util.Date;
/**
Quando quero usar os dados de pessoas para aluno, professor chamo a "pessoa" que 
 já tem os dados de cadastro que qualquer pessoa tem, facilitando e diminuindo
 linhas do codigo
*/
/**
  Informando variaveis e tipo delas
 */
public class Pessoa {
    String nome; 
    String cpf; 
    Date dataNasc;
   /**
chamando as variaveis e dando valor a elas
*/ 
    Pessoa(String nome, String cpf, Date data) { 
        this.nome = nome;
        this.cpf = cpf; 
        this.dataNasc = data;
            }
}
